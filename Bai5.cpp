//
//  bai5.cpp
//  Lab02
//
//  Created by Tran Ngoc Linh on 4/11/15.
//  Copyright (c) 2015 Tran Ngoc Linh. All rights reserved.
//

#include <iostream>
using namespace std;
void merge(int a[],int first,int last,int mid){
    int i=first;
    int j=mid+1;
    int p=first;
    int t[100];
    while(i<=mid&&j<=last){
        if(a[i]<a[j]){
            t[p]=a[i];
            p++;
            i++;
        }else{
            t[p]=a[j];
            p++;
            j++;
        }
    }
    if(j>last)
        while (i<=mid) {
            t[p]=a[i];
            p++;
            i++;
        }
    if (i>mid)
        while (j<=last) {
            t[p]=a[j];
            p++;
            j++;
        }
    for (i = first; i<=last; i++) {
        a[i]=t[i];
    }
}
void mergeSort(int a[],int first,int last){
    if (first<last) {
        int mid =(int)(first+last)/2;
        mergeSort(a, first, mid);
        mergeSort(a, mid+1, last);
        merge(a, first, last, mid);
    }
}

int main(){
    int n;
    int a[100];
    cout<<"Nhap n: ";
    cin>>n;
    cout<<"Nhap mang\n";
    for (int i=0; i<n; i++) {
        cin>>a[i];
    }
    mergeSort(a, 0, n-1);
    cout<<"---------------\n";
    for (int i=0; i<n; i++) {
        cout<<a[i]<<" ";
    }
    return 0;
}
