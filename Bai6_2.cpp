//
//  Bai6_2.cpp
//  Lab02
//
//  Created by Tran Ngoc Linh on 4/11/15.
//  Copyright (c) 2015 Tran Ngoc Linh. All rights reserved.
//

#include <iostream>
using namespace std;
bool a[100][100];
int trace[100][100];
int rows[]={-2,-2,-1,-1,1 ,1, 2,2};
int cols[]={-1, 1,-2, 2,-2,2,-1,1};
int count1=1;

void init(){
    for (int i=0; i<8; i++) {
        for (int j=0; j<8; j++) {
            a[i][j]=true;
        }
    }
}

void print(){
    for (int i=0; i<8; i++) {
        for (int j=0; j<8; j++) {
            cout<<trace[i][j]<<"\t";
            
        }
        cout<<endl;
    }
}

void quaylui(int x,int y){
    for (int i=0; i<8; i++) {
        int x1 = x+rows[i];
        int y1 = y+cols[i];
        if(x1>=0 && x1<8 && y1 >=0 && y1<8){
            if (a[x1][y1]) {
                count1++;
                trace[x1][y1]=count1;
                if(count1==8*8-1) {
                    print();
                    cout<<"------------------\n";
                    exit(-1);
                }else{
                    a[x1][y1]=false;
                    quaylui(x1, y1);
                    a[x1][y1]=true;
                    count1--;
                }
            }
        }
    }
}
int main(){
    int x,y;
    cout<<"Nhap vi tri x,y con ma:\n";
    cout<<"x = ";cin>>x;
    cout<<"y = ";cin>>y;
    init();
    trace[x][y]=1;
    a[x][y]=false;
    quaylui(x, y);
    return 0;
}
