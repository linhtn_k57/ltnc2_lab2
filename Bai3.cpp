//
//  Bai3.cpp
//  Lab02
//
//  Created by Tran Ngoc Linh on 4/10/15.
//  Copyright (c) 2015 Tran Ngoc Linh. All rights reserved.
//

#include <iostream>
using namespace std;

void swap(int *a,int *b){
    int *c;
    *c= *a;
    *a= *b;
    *b= *c;
}
void Nhap(int a[],int&n){
    cout<<"Nhap n: ";
    cin>>n;
    cout<<"Nhap mang: \n";
    for (int i = 0; i<n; i++) {
        cin>>a[i];
    }
}
void Sinh(int b[],int n){
    int i;
    int a[100];
    //init
    for(int i=0;i<n;i++){
        a[i]=i;
    }
    i=n-2;
    //generation
    while(i>=0){
        for(int i = 0;i<n;i++){
            cout<<b[a[i]]<<" ";
        }
        cout<<endl;
        i = n-2;
        while (i>=0&&a[i]>a[i+1]) {
            i--;
        }
        if(i>=0){
            int k=n-1;
            while (a[i]>a[k]) {
                k--;
            }
            swap(a[i],a[k]);
            int x=i+1;
            int y=n-1;
            while (x<y) {
                swap(a[x], a[y]);
                x++;
                y--;
            }
        }
    }
}
int main(){
    int a[100];
    int n;
    Nhap(a,n);
    Sinh(a, n);
    return 0;
}
