//
//  main.cpp
//  Lab02
//
//  Created by Tran Ngoc Linh on 4/10/15.
//  Copyright (c) 2015 Tran Ngoc Linh. All rights reserved.
//

#include <iostream>
using namespace std;
float c[100][100];

void input(float a[][100],float b[][100],int &m,int &n,int &p){
    cout<<"Nhap M,N,P :";
    cin>>m>>n>>p;
    cout<<"Nhap ma tran M*N: "<<endl;
    for(int i = 0;i<m;i++){
        for (int j= 0; j<n; j++) {
            cin>>a[i][j];
        }
    }
    cout<<"Nhap ma tran N*P: "<<endl;
    for(int i = 0;i<n;i++){
        for (int j= 0; j<p; j++) {
            cin>>b[i][j];
        }
    }
    for(int i = 0;i<m;i++){
        for (int j= 0; j<p; j++) {
            c[i][j]=0;
        }
    }
}
void tinh(float a[][100],float b[][100],int m,int n, int p){
    for(int i = 0;i<m;i++){
        for (int j =0;j<p; j++) {
            for( int k = 0 ;k<n;k++){
                c[i][j] =c[i][j]+a[i][k]*b[k][j];
            }
        }
    }
}
void output(int m,int p){
    cout<<"Ma tran ket qua cua phep nhan 2 ma tran tren la:\n";
    for (int i=0; i<m; i++) {
        for (int j= 0; j<p; j++) {
            cout<<c[i][j]<<" ";
        }
        cout<<endl;
    }
}
int main(int argc, const char * argv[]) {
    // insert code here...
    float a[100][100];
    float b[100][100];
    int n,m,p;
    input(a,b, m,n,p);
    tinh(a,b,m,n,p);
    output(m,p);
    return 0;
}
