//
//  Bai2.cpp
//  Lab02
//
//  Created by Tran Ngoc Linh on 4/10/15.
//  Copyright (c) 2015 Tran Ngoc Linh. All rights reserved.
//

#include <iostream>
using namespace std;
int UCLN(int a,int b){
    if (a%b==0) {
        return b;
    }else{
        return UCLN(b,a%b);
    }
}
void input(int a[],int &n){
    cout<<"Nhap n: ";
    cin>>n;
    cout<<"Nhap mang\n";
    for (int i= 0; i<n;i++) {
        cin>>a[i];
    }
}

void xuli(int a[],int n){
    int c = UCLN(a[0], a[1]);
    for(int i=2;i<n;i++){
        if (c == 1) {
            cout<<"UCLN la: 1"<<endl;
            break;
        }else{
            c=UCLN(c, a[i]);
        }
    }
    cout<<"UCLN la: "<<c<<endl;
}

int main(){
    int a[100];
    int n;
    input(a, n);
    xuli(a,n);
    return 0;
}
